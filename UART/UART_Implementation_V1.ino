/*
#define CLOCK_SC 16000000UL
#define DATA_SIZE 8
#define START_BIT 0x0000U
#define STOP_BIT 0x0001U // One stop bit. Two stop bits would be 0x03U
#define PARITY_BIT 'n' // No parity bit

static volatile int16_t counter = DATA_SIZE + 1 + 0 + 1;

uint16_t UBR = 0; // Countdown for baudrate

// Transmit and receive data buffer.
uint16_t T_DB = 0;
uint16_t R_DB = 0;

// Shift registers
uint16_t T_SR = 0;
uint16_t R_SR = 0;

void UART_Init(int baud)
{
  pinMode(Tx, OUTPUT);
  digitalWrite(Tx, HIGH); // active-low
  
  UBR = (CLOCK_SC / (16L * baud)) - 1;

  cli(); // disable global interrupts

  TCCR1A = 0; // Make sure the Timer/Counter1 Control Registers
  TCCR1B = 0; // are set to 0.
  
  TCCR1B |= _BV(CS10); // Run at System Clock
  TCCR1B |= _BV(WGM12); // Compare timer to OCR1A
  OCR1A = UBR; // Set the countdown value

  sei(); // enable global interrupts
}

void UART_Write(String data)
{
  uint16_t mask = 0x0001U;

  for (int queue = 0; queue < data.length(); queue++)
  {
    T_SR |= START_BIT;

    int i;
    for (i = 0; i < DATA_SIZE; i++)
    {
      T_SR |= ((mask << i) & data[queue]) << 1; // Last << 1 for the START_BIT
    }

    T_SR |= STOP_BIT << i;
  }

  counter = DATA_SIZE;
  //Serial.println(T_SR, BIN);

  cli(); // disable global interrupts
  // Enable timer compare interrupt:
  TIMSK1 |= _BV(OCIE1A);
  
  // Clear any interrupt flags.
  TIFR1 |= _BV(OCF1A);

  // Set the timer counter to 0.
  TCNT1 = 0;
  sei(); // enable global interrupts
}

ISR(TIMER1_COMPA_vect) // for CTC
{
  digitalWrite(Tx, T_SR & 0x0001U);
  //Serial.println(T_SR & 0x0001U);
  T_SR >>= 1;

  counter--;
  if (counter == -1)
  {
    cli(); // disable global interrupts
    // Disable timer compare interrupt:
    TIMSK1 &= ~_BV(OCIE1A);
    sei(); // enable global interrupts
  }
}

char UART_Read()
{
  
  return 0;
}
*/
