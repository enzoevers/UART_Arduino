/*#define CLOCK_SC 16000000UL
#define DATA_SIZE 8
#define START_BIT 0x0000U
#define STOP_BIT 0x0001U // One stop bit. Two stop bits would be 0x03U
#define PARITY_BIT 'n' // No parity bit
#define Rx_BUFFER_SIZE 64

int16_t packageSize = DATA_SIZE + 1 + 0 + 1;
static volatile int16_t TxCounter = packageSize;
static volatile int16_t RxCounter = 0;

uint16_t UBR = 0; // Countdown for baudrate

// Transmit and receive data buffer.
uint16_t T_DB = 0;
volatile uint8_t R_DB[Rx_BUFFER_SIZE];
volatile uint8_t RxBufferIndex = 0;

// Shift registers
volatile uint16_t T_SR = 0;
volatile uint8_t R_SR = 0;

volatile bool T_SR_Finished = true;
volatile bool T_SR_Filled = false;
volatile bool readingMessage = false;

volatile uint16_t OCR2A_counter_top = 0;
volatile uint16_t timerCounter = 0;

volatile uint8_t RxVal = 0;

//========================================//
//  Init                                  //
//========================================//

void UART_Init(int baud)
{
  pinMode(Tx, OUTPUT);
  digitalWrite(Tx, HIGH); // active-low

  pinMode(Rx, INPUT);

  UBR = (CLOCK_SC / (16L * baud)) - 1;
  Serial.println(UBR);

  cli(); // disable global interrupts

  //TIMER 1
  TCCR1A = 0; // Make sure the Timer/Counter1 Control Registers
  TCCR1B = 0; // are set to 0.

  TCCR1B |= _BV(CS10); // Run at System Clock
  TCCR1B |= _BV(WGM12); // Compare timer to OCR1A

  // With a system clock of 16.000.000 and a baud rate of 9600
  // the UBR value would be:
  //  (16.000.000/(16*9600)) - 1 ~= 103
  // 16.000.000 / 103 ~= 155.340
  // 1.000.000 microseconds / 155.340 ~= 6 microseconds

  // (16.000.000/9600) - 1 ~= 1665
  // 16.000.000 / 1665 ~= 9610
  // 1.000.000 microseconds / 9610 ~= 104 microseconds ~= 0.1 millisecond
  // 0.1 millisecond was seen in the logic analyzer with the normal serial Tx
  OCR1A = UBR; // Set the countdown value

  // Enable timer compare interrupt:
  TIMSK1 |= _BV(OCIE1A);

  // Clear any interrupt flags.
  TIFR1 |= _BV(OCF1A);

  // Set the timer counter to 0.
  TCNT1 = 0;

  sei(); // enable global interrupts
}

//========================================//
//  Write                                 //
//========================================//

void UART_Write(String data)
{
  for (int queue = 0; queue < data.length(); queue++)
  {
    UART_Write(data[queue]);
  }
}

void UART_Write(uint8_t data)
{
  uint16_t mask = 0x0001U;

  T_SR |= START_BIT;

  int i = 0;
  for (; i < DATA_SIZE; i++)
  {
    T_SR |= ((mask << i) & data) << 1; // Last << 1 for the START_BIT
  }

  T_SR |= STOP_BIT << (i + 1); // Shift the amount of data bits + taking into accoutn the start bit.

  TxCounter = packageSize;
  T_SR_Filled = true;

  while (T_SR_Filled) {} // If this isn't here data will be merged
}

//========================================//
//  Read                                  //
//========================================//

bool UART_Available()
{
  return (R_DB[0] != 0x00U);
}

char UART_Read()
{
  char charToReturn =  R_DB[0];

  for (int i = 0; i < RxBufferIndex; i++)
  {
    R_DB[i] = R_DB[i + 1];
  }
  R_DB[RxBufferIndex] = 0;

  RxBufferIndex--;

  return charToReturn;
}

//========================================//
//  ISR                                   //
//========================================//

//====================//
//  Shift out Tx      //
//====================//
ISR(TIMER1_COMPA_vect) // for CTC
{
  timerCounter++;

  if (timerCounter == 16)
  {
    if (T_SR_Filled)
    {
      digitalWrite(Tx, T_SR & 0x0001U);
      T_SR >>= 1;

      TxCounter--;
      if (TxCounter == 0)
      {
        T_SR_Filled = false;
        //T_SR_Finished = true;
      }
    }
    timerCounter = 0;
  }

  if (timerCounter >= 7 && timerCounter <= 9)
  {
    RxVal += digitalRead(Rx);
    if (timerCounter == 9)
    {
      // If there were more 2 or 3 one's the signal is 1.
      RxVal = (RxVal >= 2);

      //Serial.println(RxVal, BIN);
      
      if (readingMessage == true)
      {
        R_SR |= (RxVal << RxCounter);
        //Serial.println(RxVal << RxCounter, BIN);
        RxCounter++;
      }

      //readingMessage = ~readingMessage && ~RxVal; // Start reading if not already reading and a 0 was received.
      if (readingMessage == false && RxVal == 0)
      {
        readingMessage = true;
      }

      if (RxCounter == DATA_SIZE && readingMessage == true)
      {
        readingMessage = false;
        R_DB[RxBufferIndex] = R_SR;
        R_SR = 0;
        RxBufferIndex++;
        if (RxBufferIndex == Rx_BUFFER_SIZE)
        {
          RxBufferIndex = 0;
        }
        RxCounter = 0;
        
        RxVal = 0;
      }
    }
  }

  //digitalWrite(Rx, HIGH);
  //digitalWrite(Rx, LOW);
}*/

