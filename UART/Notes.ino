/*
 * http://www.atmel.com/images/atmel-8271-8-bit-avr-microcontroller-atmega48a-48pa-88a-88pa-168a-168pa-328-328p_datasheet_complete.pdf
 * 
   https://www.youtube.com/watch?v=KnyyQujvcBo
   
   UDR0 is 8 bit: the buffer for Rx and Tx???

   Baudrate = UBRR0 16 bit (*H and *L)
   An external clock can be used for synchronization.

   UCSR0A:
    - Error detection
    - Double speed
    - Mutli-process mode
    - Status flags for interrupts
   UCSR0B:
    - Receive enable (RXEN0)
    - Transmit enable (TXEN0)
    - 9th but for 9 bit data packages
    - UCSZ02 (Char size for 9 bits)
    - Interrupt enable
      = Tx complete
      = Rx complete
      = Tx data register empty
   UCSR0C
    - Mode select
    - Parity mode
    - Stop bit
    - Char size

  http://maxembedded.com/2013/09/the-usart-of-the-avr/#UBRR
  The value in the UBRR registers store the baudrates in a 12 bit version.
  The 12 bits are calculated with
        (SystemClock/(BaudRate+1)) / 16 | 8 | 2 (depending on mode)
  From the value of UBRR the system counts down with the speed of the system clock/
  When the counter reaches 0 an other bit is send.

  http://www.avrbeginners.net/architecture/uart/uart.html
*/

