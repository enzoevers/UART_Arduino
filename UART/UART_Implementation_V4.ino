#define CLOCK_SC 16000000UL
#define DATA_SIZE 8
#define START_BIT 0x0000U
#define STOP_BIT 0x0001U // One stop bit. Two stop bits would be 0x03U
#define PARITY_BIT 'n' // No parity bit
#define Rx_BUFFER_SIZE 64
#define Rx_SAMPLE_SPEED 16UL // How many times per data but Rx samples.
#define Rx_NR_SAMPLES 3 // Must be odd
//#define Rx_NR_SAMPLE_AMPLITUDE (Rx_NR_SAMPLES-1)/2

const int Rx_NR_SAMPLE_AMPLITUDE = (Rx_NR_SAMPLES - 1) / 2; // Only seemed to work if it is a variable.

int16_t packageSize = DATA_SIZE + 1 + 0 + 1;
static volatile int16_t TxCounter = packageSize; // Used for knowing how much signals need to be send ove the Tx line.
static volatile int16_t RxCounter = 0; // Used for placing the Rx signals in the right place when shifting data in.

uint32_t UBR = 0; // Countdown for baudrate

// Transmit and receive data buffer.
volatile uint8_t R_DB[Rx_BUFFER_SIZE];
volatile uint8_t RxBufferIndex = 0;

// Shift registers
volatile uint16_t T_SR = 0;
volatile uint8_t R_SR = 0;

volatile bool T_SR_Filled = false;

volatile uint8_t RxVal = 0;
volatile bool readingMessage = false;

volatile uint16_t timerCounter = 0;

int Tx = 0;
int Rx = 0;

//========================================//
//  Init                                  //
//========================================//

void UART_Init(int baud, int RxPin, int TxPin)
{
  Tx = TxPin;
  Rx = RxPin;
  pinMode(Rx, INPUT);
  pinMode(Tx, OUTPUT);
  digitalWrite(Tx, HIGH); // active-low

  UART_SetupTimer(baud);

  // Clear the Rx buffer.
  for (int i = 0; i < Rx_BUFFER_SIZE; i++)
  {
    R_DB[i] = 0;
  }
}

void UART_SetupTimer(int baud)
{
  // With a system clock of 16.000.000 and a baud rate of 9600
  // the UBR value would be:
  // (16.000.000/(16*9600)) - 1 ~= 103
  // 16.000.000 / 103 ~= 155.340
  // 1.000.000 microseconds / 155.340 ~= 6 microseconds
  //
  // The actual bit transfer over Tx is 16 times as much:
  // (16.000.000/9600) - 1 ~= 1665
  // 16.000.000 / 1665 ~= 9610
  // 1.000.000 microseconds / 9610 ~= 104 microseconds ~= 0.1 millisecond
  // 16 * 6 microseconds = 96 microseconds
  // Taking into account some rounding errors this is around 0.1 millisecond
  //
  // 0.1 millisecond was seen in the logic analyzer on the onboard serial Tx.

  // Calcuate the rate at which the Rx signal is sampled.
  UBR = (CLOCK_SC / (Rx_SAMPLE_SPEED * baud)) - 1;

  cli(); // disable global interrupts

  // SET TIMER 1
  TCCR1A = 0; // Make sure the Timer/Counter1 Control Registers
  TCCR1B = 0; // are set to 0.

  TCCR1B |= _BV(CS10); // Run at System Clock
  TCCR1B |= _BV(WGM12); // Compare timer to OCR1A

  OCR1A = UBR; // Set the countdown value

  TIMSK1 |= _BV(OCIE1A); // Enable timer compare interrupt:
  TIFR1 |= _BV(OCF1A); // Clear any interrupt flags.

  TCNT1 = 0; // Set the timer counter to 0.

  sei(); // enable global interrupts
}

//========================================//
//  Write                                 //
//========================================//

void UART_Write(String data)
{
  // For each character in the string perfom the write
  // operation of a character.
  for (int queue = 0; queue < data.length(); queue++)
  {
    UART_Write(data[queue]);
  }
}

void UART_Write(uint8_t data)
{
  uint16_t mask = 0x0001U; // The mask for setting the bits of data.

  T_SR |= START_BIT; // Shift in the START_BIT

  int i = 0; // Outside because it is used for positioning the STOP_BIT
  for (; i < DATA_SIZE; i++) // DATA_SIZE is the size of data.
  {
    T_SR |= ((mask << i) & data) << 1; // For each (mask << i)th bit in data check if it is 0 or 1.
                                       // Last '<< 1' for taking into account the START_BIT
  }

  T_SR |= STOP_BIT << (i + 1); // Shift the amount of data bits + taking into account the start bit.

  TxCounter = packageSize; // Set the total size of the package (start bit + data bits + parity bits + stop bits)
  T_SR_Filled = true; // Set the flag.

  while (T_SR_Filled) {} // Wait for T_SR to be empty.
}

//========================================//
//  Read                                  //
//========================================//

bool UART_Available()
{
  return RxBufferIndex; // Check if there is data in the first index.
}

char UART_Read()
{
  char charToReturn =  R_DB[0]; // Temporary store the first index in the buffer.

  // Shift the data in the buffer 1 index down.
  // This makes UART_Available() a valid functin.
  for (int i = 0; i < RxBufferIndex; i++)
  {
    R_DB[i] = R_DB[i + 1];
  }
  R_DB[RxBufferIndex] = 0; // The last index isn't used enymore so set it to 0.

  RxBufferIndex--; // Decrease the index for the Rx buffer.

  return charToReturn; // Return the temporary stored character.
}

//========================================//
//  ISR                                   //
//========================================//

ISR(TIMER1_COMPA_vect) // for CTC
{
  timerCounter++;

  // Check if the next data bit needs to be send out of Tx.
  if (timerCounter == Rx_SAMPLE_SPEED)
  {
    ISR_Tx_Routine();
    timerCounter = 0;
  }

  // Check if data on the Rx line needs to be sampled.
  if (timerCounter >= ((Rx_SAMPLE_SPEED / 2) - Rx_NR_SAMPLE_AMPLITUDE)
      && timerCounter <= ((Rx_SAMPLE_SPEED / 2) + Rx_NR_SAMPLE_AMPLITUDE))
  {
    ISR_Rx_Routine();
  }
}


inline void ISR_Tx_Routine()
{
  if (T_SR_Filled)
  {
    digitalWrite(Tx, T_SR & 0x0001U); // Write the next bit from T_SR.
    T_SR >>= 1; // Shift T_SR so that the next but can be next time this function is used.

    TxCounter--; // Countdown the amount of bits need to be send.
    if (TxCounter == 0)
    {
      T_SR_Filled = false;
    }
  }
}

inline void ISR_Rx_Routine()
{
  RxVal += digitalRead(Rx); // Add 3 samples if the Rx signal.
  if (timerCounter == ((Rx_SAMPLE_SPEED / 2) + Rx_NR_SAMPLE_AMPLITUDE))
  {
    // If half of the samples where 1 RxVal becomes 1, else it becomes 0.
    RxVal = (RxVal >= Rx_NR_SAMPLE_AMPLITUDE + 1);

    if (readingMessage == true)
    {
      R_SR |= (RxVal << RxCounter); // Shift in the Rx data at the correct place.
      RxCounter++;
    }

    //readingMessage = ~readingMessage && ~RxVal; // Start reading if not already reading and a 0 was received.
    if (readingMessage == false && RxVal == 0) // Check if there is new data on Rx.
    {
      readingMessage = true;
    }

    if (RxCounter == DATA_SIZE && readingMessage == true)
    {
      readingMessage = false;
      R_DB[RxBufferIndex] = R_SR; // Put the read data in the buffer.
      R_SR = 0;

      RxBufferIndex++; // Increase the buffer index

      // The oldest data (index 0) will be overwritten when de buffer is full.
      if (RxBufferIndex == Rx_BUFFER_SIZE)
      {
        RxBufferIndex = 0;
      }

      RxCounter = 0;
      RxVal = 0;
    }
  }
}
