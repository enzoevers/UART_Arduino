/*
#define  START_BIT 0
#define   STOP_BIT 1
#define PARITY_BIT 'n' // No parity

#define BUFFER_SIZE 64

uint8_t shiftRegister_R = 0; // Receive
uint8_t shiftRegister_T = 0; // Transmit

uint8_t buffer_R[BUFFER_SIZE]; // Receive
uint8_t buffer_T[BUFFER_SIZE]; // This is filled when UART_Write is called

uint8_t buffer_T_index = 0;

// SENDING 8 BITS PER SECOND
//
// Atmega328P chip has an inital clock of 16.000.000 Hz
// 1 second = 16.000.000 clock cycles
// 1 clock cycle = 1/16.000.000 = 0,0000000625 seconds = 0,0000625 milliseconds
//
// Max prescaler on Atmega328P is 256.
// 1 second = 16.000.000/256 clock cycles = 62.500 clock cycles
// 1 clock cycle = 1/62.500 = 0,000016 seconds = 0,016 milliseconds
//
// Calculate how many seconds per bit are needed for sending 8 bits per second.
// 1 second / 8 = 0,125 seconds = 125 milliseconds
// 125 ~= 128. 128 is easier to calculate with
// 128 milliseconds / 0,016 milliseconds = 8.000 cycles
//
// So for 1 byte per second the prescaller needs to be set to 256 and then wait for 8.000 cycles before sending the next bit.

unsigned int bps = 8;
uint8_t dataLength = 8;

void UART_init()
{
  pinMode(Rx, INPUT);
  pinMode(Tx, OUTPUT);

  // Tx is active-low
  digitalWrite(Tx, HIGH);

  shiftRegister_R = 0;
  shiftRegister_T = 0;
}

void UART_Write(String message)
{
  for ( buffer_T_index = 0;
        buffer_T_index < BUFFER_SIZE &&
        buffer_T_index < message.length();
        buffer_T_index++)
  {
    buffer_T[buffer_T_index] = message[buffer_T_index];
  }

  // SET PRESCALER !!!!!!!!!!

  for (int queue = 0; queue < buffer_T_index; queue++)
  {
    digitalWrite(Tx, START_BIT); // Let the receiver know data is comming

    Serial.print("Character is queue = ");
    Serial.print((char)buffer_T[queue]);
    Serial.print(" in binary = ");
    Serial.println(buffer_T[queue], BIN);


    uint8_t mask = 0x01U; // b'0000.0001
    
    for (int i = 0; i < dataLength; i++)
    {
      digitalWrite(Tx, (buffer_T[queue] & mask));
      //Serial.print((buffer_T[queue] & mask) ? '1' : '0');
      
      mask <<= 1;
    }

    Serial.println();

    digitalWrite(Tx, STOP_BIT); // Let the receiver know data is stops comming
  }

  // RESET PRESCALER !!!!!!!!!!
}

// buffer:  refence to to varaible where the received message
//          is stored in.
// Return:  number of received data bytes.
int UART_Read(String& buffer)
{
  // SET PRESCALER !!!!!!!!!!

  // RESET PRESCALER !!!!!!!!!!
}
*/

