/*
#define CLOCK_SC 16000000UL
#define DATA_SIZE 8
#define START_BIT 0x0000U
#define STOP_BIT 0x0001U // One stop bit. Two stop bits would be 0x03U
#define PARITY_BIT 'n' // No parity bit
#define Rx_BUFFER_SIZE 64

int16_t packageSize = DATA_SIZE + 1 + 0 + 1;
static volatile int16_t TxCounter = packageSize;
static volatile int16_t RxCounter = 0;

uint8_t samplingSize = 16L;
uint16_t UBR = 0; // Countdown for baudrate

// Transmit and receive data buffer.
uint16_t T_DB = 0;
uint8_t R_DB[Rx_BUFFER_SIZE];
uint8_t RxBufferIndex = 0;

// Shift registers
uint16_t T_SR = 0;
uint8_t R_SR = 0;

volatile bool T_SR_Filled = false;
volatile bool readingMessage = false;

//========================================//
//  Init                                  //
//========================================//

void UART_Init(int baud)
{
  pinMode(Tx, OUTPUT);
  digitalWrite(Tx, HIGH); // active-low

  pinMode(Rx, INPUT);

  UBR = (CLOCK_SC / baud) - 1;

  cli(); // disable global interrupts

  //TIMER 1
  TCCR1A = 0; // Make sure the Timer/Counter1 Control Registers
  TCCR1B = 0; // are set to 0.

  TCCR1B |= _BV(CS10); // Run at System Clock
  TCCR1B |= _BV(WGM12); // Compare timer to OCR1A

  // With a system clock of 16.000.000 and a baud rate of 9600
  // the UBR value would be:
  //  (16.000.000/(16*9600)) - 1 ~= 103
  // 16.000.000 / 103 ~= 155.340
  // 1.000.000 microseconds / 155.340 ~= 6 microseconds

  // (16.000.000/9600) - 1 ~= 1665
  // 16.000.000 / 1665 ~= 9610
  // 1.000.000 microseconds / 9610 ~= 104 microseconds ~= 0.1 millisecond
  // 0.1 millisecond was seen in the logic analyzer with the normal serial Tx
  OCR1A = UBR; // Set the countdown value

  // Enable timer compare interrupt:
  TIMSK1 |= _BV(OCIE1A);

  // Clear any interrupt flags.
  TIFR1 |= _BV(OCF1A);

  // Set the timer counter to 0.
  TCNT1 = 0;

  //TIMER 2
  TCCR2A = 0;
  TCCR2A = 0;

  TCCR2B |= _BV(CS20); // Run at System Clock
  TCCR2A |= _BV(WGM21); // Compare timer to OCR0A
  OCR2A = UBR / 16; // For sampling the middle of the baudrate timing
  // Enable timer compare interrupt:
  TIMSK2 |= _BV(OCIE2A);

  // Clear any interrupt flags.
  TIFR2 |= _BV(OCF2A);

  // Set the timer counter to 0.
  TCNT2 = 0;

  sei(); // enable global interrupts
}

//========================================//
//  Write                                 //
//========================================//

void UART_Write(String data)
{
  uint16_t mask = 0x0001U;

  for (int queue = 0; queue < data.length(); queue++)
  {
    T_SR |= START_BIT;

    int i;
    for (i = 0; i < DATA_SIZE; i++)
    {
      T_SR |= ((mask << i) & data[queue]) << 1; // Last << 1 for the START_BIT
    }

    T_SR |= STOP_BIT << (i + 1); // Shift the amount of data bits + taking into accoutn the start bit.
  }

  TxCounter = packageSize;
  T_SR_Filled = true;
}

//========================================//
//  Read                                  //
//========================================//

bool UART_Available()
{
  return (R_DB[0] != 0x00U);
}

char UART_Read()
{
  char charToReturn =  R_DB[0];

  for (int i = 0; i < RxBufferIndex; i++)
  {
    R_DB[i] = R_DB[i + 1];
  }
  R_DB[RxBufferIndex] = 0;

  RxBufferIndex--;

  return charToReturn;
}

//========================================//
//  ISR                                   //
//========================================//

//====================//
//  Shift out Tx      //
//====================//
ISR(TIMER1_COMPA_vect) // for CTC
{
  if (T_SR_Filled)
  {
    digitalWrite(Tx, T_SR & 0x0001U);
    T_SR >>= 1;

    TxCounter--;
    T_SR_Filled = TxCounter; // If counter is 0 T_SR_Filled becomes false.
                             // This elimenates the need for an if satement.
  }
}

//====================//
//  Shift in Rx       //
//====================//
ISR(TIMER2_COMPA_vect) // for CTC
{
  uint8_t RxVal = digitalRead(Rx);

  if (readingMessage == true)
  {
    R_SR |= (RxVal << RxCounter);
    RxCounter++;
  }

  //readingMessage = ~readingMessage && ~RxVal; // Start reading if not already reading and a 0 was received.
  if (readingMessage == false && RxVal == 0)
  {
    readingMessage = true;
  }

  if (RxCounter == DATA_SIZE)
  {
    readingMessage = false;
    R_DB[RxBufferIndex] = R_SR;
    RxBufferIndex++;
    if (RxBufferIndex == Rx_BUFFER_SIZE)
    {
      RxBufferIndex = 0;
    }
    RxCounter = 0;
  }
}
*/
