/*
    Auhtor: Enzo Evers
    Last updated: 19 sept 2017

    Description:
      This program shows in implementation of UART.
      It doesn't use any existing UART solutions for sending potmeter data to the PWM LED.
      Tab "UART_Implementation_V4" is the final code.
      The maximum baudrate that I tested and worked with some incorrect data is 7500. This was
      tested with the Arduino Uno.
*/

#include <avr/interrupt.h>

// The Arduino that uses the Write() function has potmeterPin connected.
int potmeterPin = A0;

// The Arduino that uses the Read() function has ledPin connected.
// Pin 11 is used for PWM because PWM on pin 9 and 10 use the same timer as
// I use for my UART implementation.
int ledPin = 11;

void setup()
{
  Serial.begin(9600);
  UART_Init(6600, 2, 3); // UART_Init(int baud, int RxPin, int TxPin);

  delay(20); // Give the UART some time to settle.
}

void loop()
{  
  // Uncomment the beheviour you want for the connected arduino.
  //Write();
  //Read();
  //delay(20);
}

void Read()
{
  if (UART_Available())
  {
    uint8_t readValue = UART_Read();
    Serial.println((int)readValue);
    analogWrite(ledPin, (int)readValue);
  }
}

void Write()
{
  UART_Write((uint8_t)map(analogRead(potmeterPin), 0, 1024, 0, 255));
}
